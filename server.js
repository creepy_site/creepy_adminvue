const createError = require('http-errors');
const express = require('express');
const path = require('path');
const logger = require('morgan');
const request = require('superagent');
const cookieSession = require('cookie-session');

const webpackConfig = require('./webpack.config.js')
const compiler = require('webpack')(webpackConfig)

const devMiddleware = require('webpack-dev-middleware')(compiler, {
  publicPath: '/',
})

const hotMiddleware = require('webpack-hot-middleware')(compiler)

const app = express();
// TODO: extract from project and reginerate secret
const CLIENT_ID = 'scary-admin';
const CLIENT_SECRET = 'ndjwkac12fg23vghc';

app.use(logger('dev'));
app.set('trust proxy', 1);
app.set('port', process.env.PORT || 3002);
app.set('views', path.join(__dirname, 'templates'));
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({ extended: false, limit: '20mb' }));
app.use(cookieSession({
  name: 'session',
  keys: [
    'secureone',
    'securetwo',
  ]
}))

app.use(express.static(path.join(__dirname, 'public')));

app.use(devMiddleware);
app.use(hotMiddleware);

app.post('/auth', (req, res) => {
  const {
    username, 
    password,
  } = req.body;

  request
    .post('localhost:3001/oauth/token')
    .set('Content-Type', 'application/x-www-form-urlencoded')
    .set('Authorization', 'Basic ' + new Buffer(`${CLIENT_ID}:${CLIENT_SECRET}`).toString('base64'))
    .send({
      grant_type: 'password',
      username,
      password,
    })
    .end((err, apiResponse) => {
      const { body } = apiResponse;
      if (body.accessToken) {
        req.session.oauth = body;
      }
      res.send(body);
    });
});

app.post('/logout', (req, res) => {
  delete req.session.oauth;
  res.sendStatus(200);
});

app.use((req, res) => {
  const { oauth } = req.session;
  const payload = {
    title: 'App',
    session: null,
  };
  if (oauth) {
    payload['session'] = Buffer.from(JSON.stringify(oauth)).toString('base64');
  }

  res.render('index', payload);
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

const server = require('http').createServer(app);

server.listen(app.get('port'), () => {
  console.info(`Express server listening on port ${app.get('port')}`);
});


module.exports = app;