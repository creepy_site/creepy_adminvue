import axios from 'axios';
import {
  requestDataAction,
  requestDataMutations,
  requestStateMutation,
  acSuccess,
  acFail,
  acLoading,
} from './common/actionsGeneric';
import dataWithState, {
  dataWithStateDefault,
  stateDefault,
} from './common/dataWithState';

const LOCAL_API = 'http://localhost:3002';
const API = 'http://localhost:3001';

const SET_CURRENT_STORIES_BLOCK = 'SET_CURRENT_STORIES_BLOCK';
const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';
const REQUEST_STATS = 'REQUEST_STATS';
const REQUEST_SOURCE = 'REQUEST_SOURCE';
const REQUEST_STORIES_COUNT = 'REQUEST_STORIES_COUNT';
const REQUEST_STORIES_BLOCK_SIZE = 'REQUEST_STORIES_BLOCK_SIZE';
const REQUEST_STORIES = 'REQUEST_STORIES';
const REQUEST_SET_STORY_STATUS = 'REQUEST_SET_STORY_STATUS';

function fetchStats() {
  const URL = `${API}/admin/stories/about`;

  return axios.get(URL).then(r => ({ data: r.data})).catch(error => ({ error }));
}

function fetchSources() {
  const URL = `${API}/admin/stories/sources`;

  return axios.get(URL).then(r => ({ data: r.data })).catch(error => ({ error }));
}

function fetchStoriesCount({ blockName }) {
  const URL = `${API}/admin/stories/count`;
  const params = {};
  if (blockName !== 'all') {
    params.blockName = blockName;
  }

  return axios.get(URL, { params }).then(r => ({ data: r.data })).catch(error => ({ error }));
}

function fetchStories({ block, page = 1 }) {
  const URL = `${API}/admin/stories/`;
  const params = {
    page,
  };

  if (block !== 'all') {
    params.block = block;
  }

  return axios.get(URL, { params }).then(r => ({ data: r.data })).catch(error => ({ error }));
}

function requestUpdateStoryStatus({ uID, action }) {
  const URL = `${API}/admin/stories/${uID}/${action}`;

  return axios.post(URL).then(r => ({ data: r.data })).catch(error => ({ error }));
}

const initState = {
  block: {
    current: null,
    size: dataWithStateDefault(0),
    page: 1,
    perPage: 10,
  },
  stats: dataWithState,
  source: dataWithStateDefault([]),
  count: dataWithStateDefault(0),
  stories: dataWithStateDefault([]),
  actionState: stateDefault,
};

const actions = {
  stats: requestDataAction(REQUEST_STATS, fetchStats),
  requestSource: requestDataAction(REQUEST_SOURCE, fetchSources),
  requestStoriesCount: requestDataAction(REQUEST_STORIES_COUNT, fetchStoriesCount),
  requestStories: requestDataAction(REQUEST_STORIES, fetchStories),
  requestStoriesBlockSize: requestDataAction(REQUEST_STORIES_BLOCK_SIZE, fetchStoriesCount),
  setStoryStatus: async function({ commit, dispatch, state }, payload) {
    commit(acLoading(REQUEST_SET_STORY_STATUS));
    const resievedData = await requestUpdateStoryStatus(payload);
    if (resievedData.error) {
      return commit(acFail(REQUEST_SET_STORY_STATUS))
    }
    commit(acSuccess(REQUEST_SET_STORY_STATUS), resievedData.data);
    dispatch('requestStories', {
      blockName: state.block.current,
      page: state.block.page,
    });
  },
  setCurrentBlock: function({ commit }, payload) {
    commit(SET_CURRENT_STORIES_BLOCK, payload);
  },
  setCurrentPage: function ({ commit }, payload) { commit(SET_CURRENT_PAGE, payload) },
};

const mutations = {
  ...requestDataMutations(REQUEST_STATS, 'stats'),
  ...requestDataMutations(REQUEST_SOURCE, 'source'),
  ...requestDataMutations(REQUEST_STORIES_COUNT, 'count'),
  ...requestDataMutations(REQUEST_STORIES_BLOCK_SIZE, 'block.size'),
  ...requestDataMutations(REQUEST_STORIES, 'stories'),
  ...requestStateMutation(REQUEST_SET_STORY_STATUS, 'actionState'),
  [SET_CURRENT_STORIES_BLOCK]: (state, { blockName }) => {
    state.block.current = blockName
  },
  [SET_CURRENT_PAGE]: (state, { page }) => { state.block.page = page },
};

export default {
  namespaced: true,
  state: initState,
  actions,
  mutations,
}