import get from 'lodash/get';
import {
  setLoading,
  setFail,
  setSuccess,
  setSuccessState,
} from './dataWithState';

export const LOADING = '_LOADING';
export const SUCCESS = '_SUCCESS';
export const FAIL = '_FAIL';

export const acLoading = action => action + LOADING;
export const acSuccess = action => action + SUCCESS;
export const acFail = action => action + FAIL;

export const requestDataAction = (action, fetchFn) => async function({ commit }, payload = {}) {
  commit(acLoading(action));
  const resievedData = await fetchFn(payload);
  if (resievedData.error) {
    return commit(acFail(action))
  }
  commit(acSuccess(action), resievedData.data);
};

export const requestDataMutations = (action, path) => ({
  [acLoading(action)]: state => setLoading(get(state, path + '.state')),
  [acFail(action)]: state => setFail(get(state, path + '.state')),
  [acSuccess(action)]: (state, payload) => setSuccess(get(state, path), payload),
});

export const requestStateMutation = (action, path) => ({
  [acLoading(action)]: state => setLoading(get(state, path)),
  [acFail(action)]: state => setFail(get(state, path)),
  [acSuccess(action)]: state => setSuccessState(get(state, path)),
})