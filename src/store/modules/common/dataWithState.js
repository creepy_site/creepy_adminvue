export const stateDefault = {
  loading: false,
  success: false,
  fail: false,
};

export function setSuccessState(state) {
  state.success = true;
  state.fail = false;
  state.loading = false;
}

export function setLoading(state) {
  state.success = false;
  state.fail = false;
  state.loading = true;
}

export function setSuccess(loadableData, payload) {
  setSuccessState(loadableData.state);

  loadableData.data = payload;
}

export function setFail(state) {
  state.success = false;
  state.fail = true;
  state.loading = false;
}

export const dataWithStateDefault = data => ({
  data,
  state: stateDefault,
})

export default {
  data: null,
  state: stateDefault,
};