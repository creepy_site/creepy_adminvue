const actions = {
  setPath({ commit }, { $router, path }) {
    $router.push({ 'path': path })
  },
};

const mutations = {
};

export default {
  namespaced: true,
  state: {},
  actions,
  mutations
};