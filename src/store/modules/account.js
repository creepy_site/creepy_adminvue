import axios from 'axios';
import qs from 'qs';
import { 
  setHeader,
  resetHeader,
  AUTH_KEY,
} from '@/utils//network';

const LOCAL_API = 'http://localhost:3002';
const HOST = 'http://localhost:3001';

function fetchLogin(username, password) {
  const url = `${LOCAL_API}/auth`;

  return axios({
    url,
    method: 'post',
    data: qs.stringify({
      username,
      password,
    })
  }).then(r => r.data)
}

function fetchLogout() {
  const url = `${LOCAL_API}/logout`;

  return axios({
    url,
    method: 'post',
  })
}

const initialState = {
  status: null,
  user: null,
  msg: null,
};

const actions = {
  login({ commit, dispatch }, { username, password }) {
    commit('loginRequest');
    return fetchLogin(username, password).then((data) => {
      if (!data.scope.includes('admin')) {
        commit('loginFailure', "You don't have an access");
      }
      commit('loginSuccess', data);
      setHeader(AUTH_KEY, `Bearer ${data.accessToken}`);
      dispatch('stories/stats', null, { root: true });
    }).catch(() => {
      commit('loginFailure', "Не удалось войти.");
    });
  },
  bootstrap({ commit, dispatch }) {
    const oauthData = JSON.parse(atob(window.session));
    const scriptTag = document.querySelector('[data-key=auth]');
    scriptTag.parentElement.removeChild(scriptTag);

    setHeader(AUTH_KEY, `Bearer ${oauthData.accessToken}`);
    delete window.session;
    commit('bootstrap', oauthData);
    dispatch('stories/stats', null, { root: true });
  },
  logout({ commit }, $router) {
    commit('logout');
    resetHeader();
    $router.push({ 'path': '/login' })

    return fetchLogout();
  },
};

const mutations = {
  bootstrap(state, data) {
    state.user = data;
  },
  loginRequest(state) {
    state.status = 'loading';
    state.msg = null;
  },
  loginSuccess(state, user) {
    state.status = 'sucess';
    state.msg = null;
    state.user = user;
  },
  loginFailure(state, msg) {
    state.status = 'fail';
    state.user = null;
    state.msg = msg;
  },
  logout(state) {
    state.status = null;
    state.user = null;
    state.msg = null;
  },
};

export default {
  namespaced: true,
  state: initialState,
  actions,
  mutations
};