import Vue from 'vue';
import Vuex from 'vuex';

import account from './modules/account';
import stories from './modules/stories';
import routing from './modules/routing';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    account,
    routing,
    stories,
  }
});