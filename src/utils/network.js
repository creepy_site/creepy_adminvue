import axios from 'axios';

export const AUTH_KEY = 'Authorization';

export function setHeader(key, value) {
  axios.defaults.headers.common[key] = value;
}

export function resetHeader(key) {
  delete axios.defaults.headers.common[key];
}
