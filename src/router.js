import Vue from 'vue';
import store from './store/index';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Block from './views/stories/Block.vue';

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      component: Login,
    },
    {
      path: '/',
      component: Home
    },
    {
      path: '/stories/block/:blockName?',
      component: Block,
      props: route => ({ blockName: route.params.blockName }),
    }
  ]
})

router.beforeEach((to, from, next) => {
  // Check user had login in before
  const loggedIn = store.state.account.user;
  if (loggedIn) {
    return next();
  }

  if (window.session) {
    console.log('HAS SESSION', window.session);
    store.dispatch('account/bootstrap', window.session);
    return next();
  }

  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/login'];
  const authRequired = !publicPages.includes(to.path);
  
  if (!authRequired) {
    return next();
  }
  
  return next('/login');
})

export default router;
