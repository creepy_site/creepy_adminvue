import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';
import SuiVue from 'semantic-ui-vue';
import Fragment from 'vue-fragment';

Vue.use(Fragment.Plugin)
Vue.use(SuiVue);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
